import static java.lang.Math.PI;

class AsymmetricMotion extends Dynamics {
    private final double b; // span width
    private final double yb, yphi, yp, yr, yda, ydr; // lateral force derivatives
    private final double lb, lp, lr, lda, ldr; // roll moment derivatives
    private final double nb, np, nr, nda, ndr; // yaw moment derivatives
    private double beta, phidot, pb_over_2V, pb_over_2Vdot, rb_over_2V, rb_over_2Vdot, psidot, sideslipdot;
    private static double phi;
    public static double psi, r, sideslip, betadot; // Asymmetric motion

    AsymmetricMotion(int frequency) {
        super(frequency);
        b = 13.36;
        double CL = 1.1360;
        double mub = 15.5;

        double K2X = 0.012;
        double K2Z = 0.037;
        double KXZ = 0.002;
        double K2XZ = KXZ * KXZ;

        double CYb = -0.9896;
        double Clb = -0.0772;
        double Cnb = 0.1638;
        double CYp = -0.0870;
        double Clp = -0.3444;
        double Cnp = -0.0108;
        double CYr = 0.4300;
        double Clr = 0.2800;
        double Cnr = -0.1930;
        double CYda = 0;
        double Clda = -0.2349;
        double Cnda = 0.0286;
        double CYdr = 0.3037;
        double Cldr = 0.0286;
        double Cndr = -0.1261;

        yb = (V/b) * (CYb/(2*mub));
        yphi = (V/b) * (CL/(2*mub));
        yp = (V/b) * (CYp/(2*mub));
        yr = (V/b) * ((CYr-4*mub)/(2*mub));
        yda = (V/b) * (CYda/(2*mub));
        ydr = (V/b) * (CYdr/(2*mub));

        lb = (V/b) * (Clb*K2Z + Cnb * KXZ)/(4*mub*(K2X*K2Z-K2XZ));
        lp = (V/b) * (Clp*K2Z + Cnp * KXZ)/(4*mub*(K2X*K2Z-K2XZ));
        lr = (V/b) * (Clr*K2Z + Cnr * KXZ)/(4*mub*(K2X*K2Z-K2XZ));
        lda = (V/b) * (Clda*K2Z + Cnda * KXZ)/(4*mub*(K2X*K2Z-K2XZ));
        ldr = (V/b) * (Cldr*K2Z + Cndr * KXZ)/(4*mub*(K2X*K2Z-K2XZ));

        nb = (V/b) * (Clb*KXZ + Cnb * K2X)/(4*mub*(K2X*K2Z-K2XZ));
        np = (V/b) * (Clp*KXZ + Cnp * K2X)/(4*mub*(K2X*K2Z-K2XZ));
        nr = (V/b) * (Clr*KXZ + Cnr * K2X)/(4*mub*(K2X*K2Z-K2XZ));
        nda = (V/b) * (Clda*KXZ + Cnda * K2X)/(4*mub*(K2X*K2Z-K2XZ));
        ndr = (V/b) * (Cldr*KXZ + Cndr * K2X)/(4*mub*(K2X*K2Z-K2XZ));
    }

    public void updateStateDerivative(double delta_a, double delta_r) {
        betadot = yb * beta + yphi * Math.sin(phi) + yp * pb_over_2V + yr * rb_over_2V + ydr * delta_r;
        phidot = 2 * (V/b) * pb_over_2V;
        pb_over_2Vdot = (lb * beta + lp * pb_over_2V + lr * rb_over_2V + lda * delta_a + ldr * delta_r) * ((SymmetricMotion.getAirspeed()-40)/(V-40));
        rb_over_2Vdot = nb * beta + np * pb_over_2V + nr * rb_over_2V + nda * delta_a + ndr * delta_r;
    }

    public static double getBetaDot(){
        return betadot;
    }

    public void updateSideslip() {
        sideslipdot = Math.cos(phi) * betadot -Math.sin(phi) * SymmetricMotion.getAlfadot() - 0.3*sideslip;
        sideslip = integrate(sideslip, sideslipdot);
    }

    public static double getSideslip(){
        return sideslip;
    }

    public double getBeta(){
        return beta;
    }

    public void updateState(boolean toggle){
        beta = integrate(beta, betadot);
        phi = integrate(phi, phidot);

        if(toggle){
            phi += PI;
            System.out.println("toggle in roll");
        }

        if (phi < -PI){
            phi += 2 * PI;
        } else if (phi > PI){
            phi -= 2 * PI;
        }

        pb_over_2V = integrate(pb_over_2V, pb_over_2Vdot);
        rb_over_2V = integrate(rb_over_2V, rb_over_2Vdot);
    }

    public static double getPhi(){
        return phi;
    }

    public double getPb_over_2V(){
        return pb_over_2V;
    }

    public double getRb_over_2V(){
        return rb_over_2V;
    }

    public void updatePsi(boolean toggle){
        psidot = Math.sin(phi) * SymmetricMotion.getQ() + Math.cos(phi) * Math.toDegrees(r);
        psi = integrate(psi, psidot);
        if(toggle){
            psi += PI;
            System.out.println("toggle in heading");
        }

        if (psi < 0){
            psi += 360;
        } else if (psi > 360){
            psi -= 360;
        }
    }

    public static double getPsi(){
        return psi;
    }

    public void updateR(){
        r = (2*V/b)*rb_over_2V;
    }

    public static double getR() {
        return r;
    }

    void reset(){
        beta = 0;
        betadot = 0;
        phi = 0;
        phidot = 0;
        pb_over_2V = 0;
        pb_over_2Vdot = 0;
        r = 0;
        rb_over_2V = 0;
        rb_over_2Vdot = 0;
        psi = 0;
        psidot = 0;
        sideslip = 0;
        sideslipdot = 0;

        System.out.println("Cessna roll reset");
    }

    public void rollout() {
        beta = 0;
        betadot = 0;
        phidot = phi < -0.01 ? 0.1 : phi > 0.01 ? -0.1 : 0; // rotate to level after touchdown
        phi = integrate(phi, phidot);
        pb_over_2V = 0;
        pb_over_2Vdot = 0;
        r = 0;
        rb_over_2V = 0;
        rb_over_2Vdot = 0;
        psidot = 0;
        sideslip = 0;
        sideslipdot = 0;
    }
}
