import java.awt.*;
import java.util.ArrayList;

public class Earth {
    final int npoints = 22;
    int[] polygon_x = new int[npoints];
    int[] polygon_y = new int[npoints];
    ArrayList<PitchLine> outline = new ArrayList<>();
    int radius;
    double offset;

    public Earth() {
        calculateRadiusAndOffset();
        for (double i : new double[]{0, 0.5, 1, 2, 3, 4, 5, 6, 7, 8}) { // i is the startOffset
            outline.add(new PitchLine(0,0,false, i));
        }

        outline.add(new PitchLine(0, 0, false, 60));
        updatePitchLines();
    }

    public void calculateRadiusAndOffset() {
        int altitude = (int) SymmetricMotion.getAltitude();
        if (altitude > 300) {
            radius = 9600000 / altitude;
            offset = -8.2E-4 * altitude;
        } else {
            radius = 32000;
            offset = -0.246;
        }
    }

    public void updatePitchLines(){
        for (PitchLine pitchLine : outline) {
            pitchLine.setLength((int) Math.round(Math.sqrt(radius * radius - (radius - pitchLine.startOffset * Simulator.deg2pxl) * (radius - pitchLine.startOffset * Simulator.deg2pxl))));
            pitchLine.setDistance(offset - pitchLine.startOffset);
        }
    }

    void draw(Graphics2D graphics2d, double phi){
        calculateRadiusAndOffset();
        updatePitchLines();
        graphics2d.setColor(Color.CYAN);
        graphics2d.fillRect(0, 0, Simulator.screenWidth, Simulator.screenHeight);
        graphics2d.setColor(Color.GREEN);
        if (SymmetricMotion.getTheta() > -33) {
            for (PitchLine pitchLine : outline) {
                pitchLine.getCoordinates(phi);
            }
            updatePolygon();
            graphics2d.fillPolygon(polygon_x, polygon_y, npoints);
        } else {
            graphics2d.fillRect(0, 0, Simulator.screenWidth, Simulator.screenHeight);
        }
    }

    private void updatePolygon() {
        int numberLines = outline.size();
        for (int i = 0; i < polygon_x.length; i ++){
            if (i < numberLines) {
                polygon_x[i] = outline.get(i).xl;
                polygon_y[i] = outline.get(i).yl;
            } else {
                polygon_x[i] = outline.get(2 * numberLines - 1 - i).xr;
                polygon_y[i] = outline.get(2 * numberLines - 1 - i).yr;
            }
        }
    }

}
