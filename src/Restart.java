import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by martin on 7-7-2017.
 */
class Restart {

    private final Simulator simulator;
    private static final JButton restartButton = new JButton("Start");

    Restart(Simulator simulator){
        this.simulator = simulator;
    }

    JButton makeButton() {
        restartButton.addActionListener(new Restarter());
        restartButton.setSize(150,50);

        return restartButton;
    }

    private class Restarter implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Rudder.deflection = 0;
            Thrust.setting = 67;
            simulator.aileron.deflection = 0;
            simulator.elevator.deflection = 0;
            simulator.pitchDynamics.reset();
            simulator.rollDynamics.reset();

            if (restartButton.getText().equals("Start") || restartButton.getText().equals("Restart")) {
                restartButton.setText("Stop");
                simulator.getTimer().start();
            } else if (restartButton.getText().equals("Stop")){
                simulator.getTimer().stop();
                restartButton.setText("Restart");
            }
        }
    }

}