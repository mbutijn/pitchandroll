import java.awt.*;

class PitchLine extends IndicationLine{
    public int xl, xr, yl, yr;
    public static int xm, ym;
    private final String pitchAngle;
    private final boolean show;
    public final double startOffset;

    PitchLine(int length, double distance, boolean show, double startOffset){
        super(length, distance);
        pitchAngle = Integer.toString((int) distance);
        this.show = show;
        this.startOffset = startOffset;
    }

    public static void updateMiddleHorizon(double phi) {
        xm = Simulator.midX + (int) (Math.sin(phi) * SymmetricMotion.getTheta() * Simulator.deg2pxl); // Shift pitch ladder with the pitch angle
        ym = Simulator.midY + (int) (Math.cos(phi) * SymmetricMotion.getTheta() * Simulator.deg2pxl); // ... in direction of phi
    }

    void draw(Graphics2D graphics2d, double phi){
        getCoordinates(phi);
        graphics2d.drawLine(xl, yl, xr, yr);

        // Write the pitch angle
        if (show) {
            graphics2d.drawString(pitchAngle, xl-20, yl);
            graphics2d.drawString(pitchAngle, xr+5, yr);
        }
    }

    public void getCoordinates(double phi){
        xl = (int) (xm - length * Math.cos(phi) - distance * Simulator.deg2pxl * Math.sin(phi)); // left
        xr = (int) (xm + length * Math.cos(phi) - distance * Simulator.deg2pxl * Math.sin(phi)); // right
        yl = (int) (ym + length * Math.sin(phi) - distance * Simulator.deg2pxl * Math.cos(phi));
        yr = (int) (ym - length * Math.sin(phi) - distance * Simulator.deg2pxl * Math.cos(phi));
    }

    public void setLength(int length){
        this.length = length;
    }

    void drawHorizon(Graphics2D graphics2d, double phi){
        getCoordinates(phi);

        graphics2d.setColor(Color.black);
        graphics2d.drawLine(xl, yl, xr, yr); // horizon on pitch ladder
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
