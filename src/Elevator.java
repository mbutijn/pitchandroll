import java.awt.*;

public class Elevator extends ControlSurface{
    int y_mid;

    public Elevator(Simulator simulator, int upperLimit, int lowerLimit) {
        super(simulator, upperLimit, lowerLimit);
        recalculateUIPositions();
    }

    public void recalculateUIPositions(){
        x_right = Simulator.screenWidth - 50; // = 950;
        x_left = x_right - 20; // = 930;
        y_bottom = Simulator.screenHeight - 230; // = 670;
        y_top = y_bottom - 150; // = 520;
        y_mid = y_bottom - 75; // = 595;

        upperLimit = Simulator.midY + 300;
        lowerLimit = Simulator.midY - 300;
    }

    public double getDeflection() {
        double mouseY = getPoint().getY() - simulator.getY();

        if (mouseY < lowerLimit) {
            mouseY = lowerLimit;
        } else if (mouseY > upperLimit) {
            mouseY = upperLimit;
        }
        if (ControlSignal.isControlling()) {
            deflection = -0.015 * (mouseY - Simulator.midY); // from -4.5 to +4.5
        }
        return deflection;
    }

    public void draw(Graphics2D graphics2d) {
        // Draw elevator input
        graphics2d.drawLine(x_left, y_top, x_left, y_bottom);
        int elevator = y_mid - (int) (16.6667 * this.deflection);
        graphics2d.drawLine(x_left, elevator, x_right, elevator);
    }
}
