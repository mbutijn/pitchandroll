import java.awt.*;

public class Aileron extends ControlSurface {
    int x_mid;

    public Aileron(Simulator simulator, int upperLimit, int lowerLimit){
        super(simulator, upperLimit, lowerLimit);
        recalculateUIPositions();
    }

    public void recalculateUIPositions(){
        x_right = Simulator.screenWidth - 50; // = 950;
        x_left = x_right - 200; // = 750;
        x_mid = x_right - 100; // = 850;
        y_bottom = Simulator.screenHeight - 180; // = 720;
        y_top = y_bottom - 20; // = 700;

//        upperLimit = Simulator.screenWidth;
        upperLimit = Simulator.midX + 400;
        lowerLimit = Simulator.midX - 400;
    }

    double getDeflection() {
        double mouseX = getPoint().getX() - simulator.getX(); // from 0 to 600

        if (mouseX < lowerLimit) {
            mouseX = lowerLimit;
        } else if (mouseX > upperLimit){
            mouseX = upperLimit;
        }
        if (ControlSignal.isControlling()) {
            deflection = -0.0005 * (mouseX - Simulator.midX); // from -0.2 to +0.2
        }
        return deflection;
    }

    public void draw(Graphics2D graphics2d){
        graphics2d.drawLine(x_left, y_top, x_right, y_top);
        int aileron = x_mid - (int) (500 * this.deflection);
        graphics2d.drawLine(aileron, y_top, aileron, y_bottom);
    }
}
