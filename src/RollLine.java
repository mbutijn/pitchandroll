import java.awt.*;

class RollLine extends IndicationLine{
    private final String rollAngle;

    RollLine(int length, int distance){
        super(length, distance);
        rollAngle = Integer.toString(Math.abs(Math.round(distance)));
        this.distance = Math.toRadians(distance);
    }

    void draw(Graphics2D graphics2d, int radius, double phi) {
        int x1 = (int) (Simulator.midX - radius * Math.sin(phi + distance));
        int x2 = (int) (Simulator.midX - (radius + length) * Math.sin(phi + distance));
        int y1 = (int) (Simulator.midY - radius * Math.cos(phi + distance));
        int y2 = (int) (Simulator.midY - (radius + length) * Math.cos(phi + distance));
        graphics2d.drawLine(x1, y1, x2, y2);

        // Write roll angle as string
        graphics2d.drawString(rollAngle, x2, y2);
    }
}
