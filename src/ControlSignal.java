import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by martin on 6-7-2017.
 */
class ControlSignal implements KeyListener {
    private final Simulator simulator;
    private static boolean controlling = true;

    ControlSignal(Simulator simulator) {
        this.simulator = simulator;
        simulator.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controlling = !controlling;
                if (controlling) {
                    Simulator.setControlMessage("");
                } else {
                    Simulator.setControlMessage(", Click on the screen for mouse control");
                }
                System.out.println("controlling pitch and roll: " + controlling);
            }
        });

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        Timer timer = simulator.getTimer();
        int code = e.getKeyCode();
        if (code == KeyEvent.VK_RIGHT){
            Simulator.rudder.steerRight();
        } else if (code == KeyEvent.VK_LEFT){
            Simulator.rudder.steerLeft();
        } else if ((code == KeyEvent.VK_PAGE_UP || code == KeyEvent.VK_9)){
            Thrust.increase();
        } else if ((code == KeyEvent.VK_PAGE_DOWN || code == KeyEvent.VK_3)){
            Thrust.decrease();
        } else if (code == KeyEvent.VK_SPACE) {
            if (timer.isRunning()) {
                timer.stop();
            } else {
                timer.start();
            }
        } else if (code == KeyEvent.VK_5){
            simulator.aileron.deflection = 0;
            simulator.elevator.deflection = 0;
            Rudder.deflection = 0;
            System.out.println("reset elevator, aileron and rudder to middle");
        } else if (code == KeyEvent.VK_G && timer.isRunning()) {
            SymmetricMotion.gear = !SymmetricMotion.gear;
        } else if (code == KeyEvent.VK_CLOSE_BRACKET || code == KeyEvent.VK_F){
            if (timer.isRunning()) {
                SymmetricMotion.increaseFlapsSetting();
            }
        } else if (code == KeyEvent.VK_OPEN_BRACKET) {
            if (timer.isRunning()) {
                SymmetricMotion.decreaseFlapsSetting();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    static boolean isControlling() {
        return controlling;
    }

}