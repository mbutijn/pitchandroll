import java.awt.*;

/**
 * Created by martin on 6-7-2017.
 */
class AircraftSymbol {
    private int left1,left2,left3,right1,right2,right3, height;
    private static int midY, midX, x_flightPath, y_flightPath;

    AircraftSymbol(int width){
        recalculateUIPositions(width);
    }

    public void recalculateUIPositions(int width){
        //int width = 311;
        int width1 = (int) Math.round(0.73955 * width); // = 230;
        int width2 = (int) Math.round(0.09646 * width); // = 30;
        int width3 = (int) Math.round(0.03215 * width); // = 10;
        midY = Simulator.midY; // = 450;
        midX = Simulator.midX; // = 485;

        left1 = midX - width1; // = 485 - 200 = 285;
        left2 = midX - width2; // = 485 - 30 = 455;
        left3 = midX - width3; // = 485 - 10 = 475;

        right1 = midX + width3; // 485 + 10 = 495;
        right2 = midX + width2; // 485 + 30 = 515;
        right3 = midX + width1; // 485 + 200 = 685;

        height = (int) Math.round(0.06431 * width); // = 20;
    }

    void makePitchSymbol(Graphics2D graphics2d) {
        graphics2d.drawLine(left1, midY, left2, midY);
        graphics2d.drawLine(right2, midY, right3, midY);

        graphics2d.drawLine(left2, midY, left2, midY + height);
        graphics2d.drawLine(right2, midY, right2, midY + height);

        graphics2d.drawLine(left3, midY, right1, midY);
    }

    static void updateFlightPathIndicator(double phi){
        double alfa_g = SymmetricMotion.getAlfa_g();
        double beta_g = AsymmetricMotion.getSideslip();
        x_flightPath = (int) Math.round(midX - 50 + Simulator.deg2pxl * (Math.cos(phi) * beta_g + Math.sin(phi) * alfa_g));
        y_flightPath = (int) Math.round(midY + Simulator.deg2pxl * (-Math.sin(phi) * beta_g + Math.cos(phi) * alfa_g));
    }

    void drawFlightPathIndicator(Graphics2D graphics2d){
        graphics2d.drawLine(x_flightPath, y_flightPath, x_flightPath + 100, y_flightPath);
    }

}