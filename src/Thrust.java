import java.awt.*;

public class Thrust {
    static int setting = 67;
    int x_left, y_top, size, x_label, y_label, x_pivot, y_pivot, length;

    public Thrust(){
        y_top = 85;
        size = 100;
        y_label = 180;
        length = 50;
        y_pivot = 135;
        recalculateUIPositions();
    }

    public static void increase() {
        if (setting < 100){
            setting++;
        }
    }

    public static void decrease() {
        if (setting > 0){
            setting--;
        }
    }

    public void recalculateUIPositions(){
        x_left = Simulator.screenWidth - 150; // = 850;
        x_label = Simulator.screenWidth - 105; // = 895;
        x_pivot = x_left + length; // = 900;
    }

    public static double getSetting() {
        return setting;
    }

    public void draw(Graphics2D graphics2d) {
        double level_offset = 0.015 * setting * Math.PI - 1.25 * Math.PI;
        graphics2d.drawArc(x_left, y_top, size, size, -45, 270);
        graphics2d.drawLine(x_pivot, y_pivot,  x_pivot + (int) (length * Math.cos(level_offset)), y_pivot + (int) (length * Math.sin(level_offset)));
        graphics2d.drawString(Integer.toString(setting)+'%', x_label, y_label);
    }

}
