class SymmetricMotion extends Dynamics {

    private final double c; // chord length
    private final double xu, xa, xt, xq, xde, xdt; // horizontal force derivatives
    private final double zu, za, zt, zq, zde, zdt; // vertical force derivatives
    private final double mu, ma, mt, mq, mde, mdt; // pitch moment derivatives
    private double udot, alfa, thetadot, qc_over_V, qc_over_Vdot;
    public static double theta, altitude, q, alfadot, alfa_g;
    private static double u, airspeed, climbrate;
    public static int flaps_index, startAltitude = 3048; // m (10000 feet)
    public double alfa_gdot; // rate of angle of attack in geo-reference frame
    public static boolean gear;
    private double deltaL = 0, deltaD = 0, deltaM = 0;

    SymmetricMotion(int frequency) {
        super(frequency);
        c = 2.022;
        double twmuc = 2 * 102.7;
        double KY2 = 0.980;

        double CX0 = 0;
        double CZ0 = -1.1360;
        double Cm0 = 0;
        double CXu = -0.2199;
        double CZu = -2.2720; //-0.3; (prevents pitch up after speed increase)
        double Cmu = 0;
        double CXa = 0.4653;
        double CZa = -5.1600;
        double Cma = -0.4300;
        double CXfa = 0;
        double CZfa = -1.4300;
        double Cmfa = -3.7000;
        double CXq = 0;
        double CZq = -3.8600;
        double Cmq = -7.0400;
        double CXde = 0;
        double CZde = -0.6238;
        double Cmde = -1.5530;
        double CXdt = 0.5;
        double CZdt = 0.1;
        double Cmdt = 0.1;

        xu  = (V/c)*(CXu/twmuc); // = -0.04765255028185578
        xa  = (V/c)*(CXa/twmuc);
        xt  = (V/c)*(CZ0/twmuc);
        xq  = (V/c)*(CXq/twmuc);
        xde = (V/c)*(CXde /twmuc);
        xdt = (V/c)*(CXdt /twmuc); // = 0.10835050086824868

        zu  = (V/c)*( CZu/(twmuc-CZfa));
        za  = (V/c)*( CZa/(twmuc-CZfa));
        zt  = (V/c)*(-CX0/(twmuc-CZfa));
        zq  = (V/c)*((CZq+twmuc)/(twmuc-CZfa));
        zde = (V/c)*(CZde /(twmuc-CZfa));
        zdt = (V/c)*(CZdt /(twmuc-CZfa));

        mu  = (V/c)*((Cmu+CZu*Cmfa/(twmuc-CZfa))/(twmuc*KY2));
        ma  = (V/c)*((Cma+CZa*Cmfa/(twmuc-CZfa))/(twmuc*KY2));
        mt  = (V/c)*((-CX0*Cmfa/(twmuc-CZfa))/(twmuc*KY2));
        mq  = (V/c)*(Cmq+Cmfa*(twmuc+CZq)/(twmuc-CZfa))/(twmuc*KY2);
        mde = (V/c)*((Cmde + CZde *Cmfa/(twmuc-CZfa))/(twmuc*KY2));
        mdt = (V/c)*((Cmdt + CZdt *Cmfa/(twmuc-CZfa))/(twmuc*KY2));

        reset();
    }

    public double getU(){
        return u;
    }

    public void updateAlfa_g(double phi) {
        double betadot_b = AsymmetricMotion.getBetaDot();
        if (alfa_g > 10 || alfa_g < -2) {
            alfa_gdot = -Math.sin(phi) * betadot_b + Math.cos(phi) * alfadot - 0.2 * alfa_g;
        } else {
            alfa_gdot = -Math.sin(phi) * betadot_b + Math.cos(phi) * alfadot;
        }
        alfa_g = integrate(alfa_g, alfa_gdot);
    }

    public static double getAlfa_g(){
        return alfa_g;
    }

    public static double getAlfadot(){
        return alfadot;
    }

    public void updateStateDerivative(double delta_e, double delta_t, double phi){
        udot = xu * u + xa * alfa + xt * theta + xq * qc_over_V + xde * delta_e + xdt * delta_t + deltaD;
        alfadot = zu * u + za * alfa + zt * theta + zq * qc_over_V + zde * delta_e + zdt * delta_t + deltaL;
        qc_over_Vdot = (mu * u + ma * alfa + mdt * delta_t + mt * theta) * Math.cos(phi) + mq * qc_over_V + mde * delta_e + deltaM;
        thetadot = Math.cos(phi) * q - Math.sin(phi) * AsymmetricMotion.getR();
    }
    
    public boolean checkTogglePitch(){
        return theta > 90 || theta < -90;
    }
    
    public void updateQ(){
        q = (V/c)*qc_over_V;
    }

    public void updateState(boolean toggle){
        u = integrate(u, udot);
        alfa = integrate(alfa, alfadot);

        if (toggle) {
            alfa = 0;
        }
        qc_over_V = integrate(qc_over_V, qc_over_Vdot);
        theta = integrate(theta, thetadot);

        if (toggle){
            thetadot *= -1;
            if (theta > 0){
                theta = 180 - theta;
            } else {
                theta = -180 - theta;
            }
        }
    }

    public static double getQ(){
        return q;
    }

    public static double getTheta() {
        return theta;
    }

    public void updateClimbRate() {
        climbrate = Math.sin(Math.toRadians(theta - alfa_g)) * airspeed;
    }

    public static double getClimbRate(){
        return climbrate;
    }

    public void updateAltitude(){
        altitude = integrate(altitude, climbrate);
    }

    public static double getAltitude() {
        return altitude;
    }

    public static void updateAirspeed() {
        u = u < -V ? -V : u;
        airspeed = V + u;
    }

    public static double getAirspeed() {
        return airspeed;
    }

    public static void increaseFlapsSetting(){
        if (flaps_index < 5) {
            flaps_index++;
            System.out.println("flaps increased");
        }
    }

    public static void decreaseFlapsSetting(){
        if (flaps_index > 0) {
            flaps_index--;
            System.out.println("flaps decreased");
        }
    }

    public void updateGearAndFlapForcesAndMoments() {
        double V2 = airspeed * airspeed;
        double deltaCL = 0;
        double deltaCM;
        double deltaCD;

        // Set the changes in forces and moments coefficients
        if (gear) {
            deltaCD = -0.0005;
            deltaCM = -0.00005;
        } else {
            deltaCD = 0;
            deltaCM = 0;
        }

        // Set the changes in flap force coefficients
        switch (flaps_index) {
            case 0: // flaps = 0
                break;
            case 1: // flaps = 2
                deltaCL = -0.0004;
                deltaCD -= 0.00004; // => delta_CL / delta_CD ratio = 10
                break;
            case 2: // flaps = 5
                deltaCL = -0.0008;
                deltaCD -= 0.0001; // => delta_CL / delta_CD ratio = 8
                break;
            case 3: // flaps = 15
                deltaCL = -0.002;
                deltaCD -= 0.0005; // => delta_CL / delta_CD ratio = 6
                break;
            case 4: // flaps = 25
                deltaCL = -0.003;
                deltaCD -= 0.00075; // => delta_CL / delta_CD ratio = 4
                break;
            case 5: // flaps = 40
                deltaCL = -0.005;
                deltaCD -= 0.00125; // => delta_CL / delta_CD ratio = 4
                break;
        }

        // Set the changes in forces and moments
        deltaL = V2 * deltaCL;
        deltaD = V2 * deltaCD;
        deltaM = V2 * deltaCM;
    }

    void reset() {
        altitude = startAltitude;

        u = 0;
        udot = 0;
        alfa = 0;
        alfadot = 0;
        theta = 0;
        thetadot = 0;
        q = 0;
        qc_over_V = 0;
        qc_over_Vdot = 0;
        alfa_g = 0;
        alfa_gdot = 0;

        // Reset forces and moments
        deltaL = 0;
        deltaD = 0;
        deltaM = 0;
        flaps_index = 0;

        gear = false;
        System.out.println("Cessna pitch reset");
    }

    public boolean checkLanded() {
        if (altitude < 2) {
            double roll = AsymmetricMotion.getPhi();
            System.out.printf("climbrate: %.1f feet/min%n", climbrate * FlightData.MPS_TO_FEETPMIN);
            System.out.printf("roll angle: %.1f deg%n", Math.toDegrees(roll));
            if (gear && climbrate > -4 && airspeed < 52 && roll > -0.174533 && roll < 0.174533){ // => V < 101 knts & roll < 10 deg
                System.out.println("The plane landed");
                return true;
            } else {
                System.out.println("The plane crashed");
                reset();
            }
        }
        return false;
    }

    public void rollout() { // thrust goes from -67 to +33
        theta = theta > 0 && airspeed > 30 ? theta : 0;
        alfa = theta;
        alfa_g = theta;
        altitude = 3;

        if (airspeed > 54 && theta > 5) { // Take off procedure
            System.out.println("Take off:");
            System.out.printf("airspeed: %.1f m/s%n", airspeed);

            Simulator.landed = false;
        }
    }

}
