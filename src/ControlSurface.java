import java.awt.*;

public class ControlSurface {
    protected double deflection;
    Simulator simulator;
    protected int upperLimit, lowerLimit;
    int x_right, y_bottom, y_top, x_left;

    public ControlSurface(Simulator simulator, int upperLimit, int lowerLimit){
        this.simulator = simulator;
        this.upperLimit = upperLimit;
        this.lowerLimit = lowerLimit;
    }

    protected Point getPoint(){
        return MouseInfo.getPointerInfo().getLocation();
    }

}
