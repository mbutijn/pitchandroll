import java.awt.*;

public class FlightData {
    private static final double M_TO_FEET = 3.2808, MPS_TO_KNTS = 1.9438;
    private static int center_airspeed;// = 650;
    public static final double MPS_TO_FEETPMIN = 196.850;

    public static void recalculateUIPositions(){
        center_airspeed = Simulator.screenHeight - 250;
    }

    public static void draw(Graphics2D graphics2d){
        // Draw the altitude indication
        double altitude = SymmetricMotion.getAltitude() * M_TO_FEET;
        makeLadder(altitude, 120, graphics2d,200, 50, 1);
        graphics2d.drawString(String.format("%.0f feet",altitude), 85, 200);

        // Draw airspeed ladder
        double speed = SymmetricMotion.getAirspeed() * MPS_TO_KNTS;
        makeLadder(speed, 20, graphics2d, center_airspeed, 10, 6);
        graphics2d.drawString(String.format("%.0f knts", speed), 70, center_airspeed);

        // Write climb rate
        graphics2d.drawString(String.format("%.0f feet/min", SymmetricMotion.getClimbRate()*MPS_TO_FEETPMIN),30, 70);
    }

    private static void makeLadder(double value, int bound, Graphics2D graphics2d, int center, int spacing, int pixelSpace){
        int limit = (int) Math.round(value);

        // Draw moving elements
        for (int i = limit - bound; i < limit + bound + 1; i++) {
            if (i >= 0 && i % spacing == 0) {
                int reference = (int) (center - (i - value) * pixelSpace);
                graphics2d.drawLine(25, reference, 35, reference);
                graphics2d.drawString(Integer.toString(i),40, reference);
            }
        }
        // Draw static elements
        graphics2d.drawLine(25, center - 125, 25, center + 125);
        graphics2d.drawLine(20, center - 5, 25, center);
        graphics2d.drawLine(20, center + 5, 25, center);
    }
}
