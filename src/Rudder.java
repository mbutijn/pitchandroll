import java.awt.*;

public class Rudder extends ControlSurface{
    static int deflection;
    int x_mid;

    public Rudder(Simulator simulator, int upperLimit, int lowerLimit){
        super(simulator, upperLimit, lowerLimit);
        recalculateUIPositions();
    }

    public void steerRight() {
        if (deflection < upperLimit) {
            deflection++;
        }
    }

    public void steerLeft(){
        if (deflection > lowerLimit) {
            deflection--;
        }
    }

    public void recalculateUIPositions(){
        x_right = Simulator.screenWidth - 50; // = 950
        x_left = x_right - 200; // = 750
        x_mid = x_right - 100; // = 850
        y_bottom = Simulator.screenHeight - 120; // = 780
        y_top = y_bottom - 20; // = 760
    }

    public static double getDeflection(){
        return -0.05 * deflection;
    }

    public void draw(Graphics2D graphics2d) {
        graphics2d.drawLine(x_left, y_top, x_right, y_top);
        graphics2d.drawLine(x_mid + 2 * deflection, y_top, x_mid + 2 * deflection, y_bottom);
    }
}
