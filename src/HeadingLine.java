import java.awt.*;

public class HeadingLine extends IndicationLine{
    private final String headingAngle;

    HeadingLine(int length, int distance){
        super(length, distance);
        headingAngle = Integer.toString(distance);
    }

    public void draw(Graphics2D graphics2d, double psi, int height){
        int offset = 0;
        if (psi < 40 && distance > 320){
            offset = -360;
        } else if (psi > 320 && distance < 40){
            offset = 360;
        }
        int x = (int) (Simulator.midX + (distance + offset - psi) * Simulator.deg2pxl);

        graphics2d.drawLine(x, height, x, height - length);

        // Write the heading angle
        graphics2d.drawString(headingAngle, x, height - length);
    }
}
