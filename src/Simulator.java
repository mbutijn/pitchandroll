import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;

public class Simulator extends JFrame {
    private static final int SAMPLE_FREQUENCY = 100; // Hertz
    static int screenHeight = 600, screenWidth = 800, midX, midY, y_write_flaps, y_write_gear, headingLineHeight, x_write_flaps_gear = 75, deg2pxl = 20; // One degree is 20 pixels
    private int rollArcSize, leftRollArc, topRollArc, rollArcRadius; // Dimensions roll arc
    private AircraftSymbol aircraftSymbol;
    private Timer timer;
    SymmetricMotion pitchDynamics = new SymmetricMotion(SAMPLE_FREQUENCY);
    AsymmetricMotion rollDynamics = new AsymmetricMotion(SAMPLE_FREQUENCY);
    private static String controlMessage = "";
    private final TextField textField = new TextField("                                                                                      ");
    private double phi; // Roll angle
    protected final static ArrayList<PitchLine> pitchLines = new ArrayList<>();
    private final ArrayList<RollLine> rollLines = new ArrayList<>();
    private final ArrayList<HeadingLine> headingLines = new ArrayList<>();
    public Earth earth = new Earth();
    public Aileron aileron;
    public Elevator elevator;
    public static Rudder rudder;
    public Thrust thrust;
    private PitchLine horizon;
    private final InterfaceComponents interfaceComponents = new InterfaceComponents();
    public static boolean landed;
    private final static int[] flaps = {0,2,5,15,25,40};

    public static void main (String[] arg){
        new Simulator().makeUI();
    }

    private void makeUI(){
        setTitle("Cessna flight simulator");

        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(screenWidth, screenHeight);
        getContentPane().add(BorderLayout.CENTER, interfaceComponents);
        recalculateUIPosition();

        textField.setEditable(false);
        setKeyBoardListeners(new ControlSignal(this));
        aircraftSymbol = new AircraftSymbol(rollArcRadius);

        // Add the control surfaces
        elevator = new Elevator(this, screenHeight, 0);
        aileron = new Aileron(this, screenWidth, 0);

        // Add pitch ladder
        horizon = new PitchLine(2000, 0, false, 0);
        addPitchLines(-90.0, 90.0, 10, 200, true); // every 10 degrees
        addPitchLines(-55.0, 55.0, 10, 133, true); // every 5 degrees
        addPitchLines(-22.5, 22.5, 5, 67, false); // every 2.5 degrees

        // Add roll indicator
        addRollLines(-80, 80, 20, 40);
        addRollLines(-70, 70, 20, 20);

        // Add heading indicator
        addHeadingLines(0, 270, 90, 50);
        addHeadingLines(10, 350, 10, 20);

        // Add the rudder
        rudder = new Rudder(this, 50, -50);

        // Add the thrust lever
        thrust = new Thrust();

        JPanel buttonPanel = new JPanel();
        JButton restart = new Restart(this).makeButton();
        restart.setFocusable(false);
        buttonPanel.add(restart);

        JPanel northPanel = new JPanel();
        northPanel.add(textField);
        northPanel.add(buttonPanel);

        getContentPane().add(BorderLayout.NORTH, northPanel);
        PitchLine.updateMiddleHorizon(0);

        timer = new Timer(1000 / SAMPLE_FREQUENCY, actionListener);
        timer.setRepeats(true);

        addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent evt) {
                recalculateUIPosition();
                elevator.recalculateUIPositions();
                aileron.recalculateUIPositions();
                rudder.recalculateUIPositions();
                thrust.recalculateUIPositions();
                PitchLine.updateMiddleHorizon(phi);
                aircraftSymbol.recalculateUIPositions(rollArcRadius);
            }
        });

    }

    private void setKeyBoardListeners(ControlSignal cs) {
        setFocusable(true);
        requestFocusInWindow();
        addKeyListener(cs);
    }

    private void addPitchLines(double start, double end, int spacing, int length, boolean show){
        for (double pitch = start; pitch < end + spacing; pitch += spacing){
            if (pitch != 0) {
                pitchLines.add(new PitchLine(length, pitch, show, 0));
            }
        }
    }

    private void addRollLines(int start, int end, int spacing, int length){
        for (int roll = start; roll < end + spacing; roll += spacing){
            rollLines.add(new RollLine(length, roll));
        }
    }

    private void addHeadingLines(int start, int end, int spacing, int length){
        for (int heading = start; heading < end + spacing + spacing; heading += spacing){
            headingLines.add(new HeadingLine(length, heading));
        }
    }

    private void recalculateUIPosition() {
        screenHeight = getHeight(); // = 900;
        screenWidth = getWidth(); // = 1000;

        midX = screenWidth / 2; // = 500;
        midY = screenHeight / 2; // = 450;
        rollArcSize = screenHeight > 0.9 * screenWidth ? (int) Math.round(0.622 * screenWidth) : (int) Math.round(0.69111 * screenHeight); // = 622;

        rollArcRadius = rollArcSize / 2; // = 311;
        leftRollArc = midX - rollArcRadius; // = 173;
        topRollArc = midY - rollArcRadius; // = 138;

        y_write_flaps = (int) Math.round(0.5 * screenHeight); // = 450;
        y_write_gear = (int) Math.round(0.5222 * screenHeight); // = 470;

        headingLineHeight = (int) Math.round(0.05 * screenHeight); // = 50;

        FlightData.recalculateUIPositions();
    }

    private final ActionListener actionListener = new ActionListener() {
        public void actionPerformed(ActionEvent evt) {
            // Get the controls
            double elevator_def = elevator.getDeflection(); // symmetric control surface inputs
            double aileron_def = aileron.getDeflection(); // asymmetric control surface inputs
            double rudder_def = Rudder.getDeflection();
            double thrust = Thrust.getSetting() - 67; // thrust input after correcting

            if (landed) {// land procedure
                pitchDynamics.rollout();
                rollDynamics.rollout();
            } else {
                landed = pitchDynamics.checkLanded();
            }

            // Evaluate symmetric EOM
            boolean togglePitch = pitchDynamics.checkTogglePitch();
            pitchDynamics.updateStateDerivative(elevator_def, thrust, phi);
            pitchDynamics.updateState(togglePitch);
            pitchDynamics.updateQ();

            // Update flight data indicators
            pitchDynamics.updateClimbRate();
            pitchDynamics.updateAltitude();

            // Evaluate asymmetric EOM
            rollDynamics.updateStateDerivative(aileron_def, rudder_def);
            rollDynamics.updateState(togglePitch);
            rollDynamics.updateR();
            rollDynamics.updatePsi(togglePitch);

            // Get angles in geo-reference frame
            pitchDynamics.updateAlfa_g(phi);
            rollDynamics.updateSideslip();

            pitchDynamics.updateGearAndFlapForcesAndMoments();

            phi = AsymmetricMotion.getPhi();

            // Update remaining parameters
            SymmetricMotion.updateAirspeed();
            PitchLine.updateMiddleHorizon(phi);
            AircraftSymbol.updateFlightPathIndicator(phi);

            // Update the text field
            textField.setText(String.format("Control input: δ_e = %.1f°; δ_a = %.1f°; δ_r = %.1f°%s",
                    elevator_def, aileron_def, rudder_def, getControlMessage()));

            // Repaint
            interfaceComponents.repaint();
        }
    };

    Timer getTimer() {
        return timer;
    }

    private class InterfaceComponents extends JPanel {
        @Override
        public void paintComponent(Graphics graphics){
            super.paintComponent(graphics);
            Graphics2D graphics2d = (Graphics2D) graphics;
            graphics2d.setColor(Color.black);

            // Draw the earth
            earth.draw(graphics2d, phi);

            // Draw the horizon
            horizon.drawHorizon(graphics2d, phi);

            // Draw the pitch angle indications
            for (PitchLine pitchLine : pitchLines){
                pitchLine.draw(graphics2d, phi);
            }

            // Draw roll indication
            graphics2d.drawArc(leftRollArc, topRollArc, rollArcSize, rollArcSize, (int)(Math.toDegrees(phi)) + 10, 160);
            graphics2d.drawLine(midX-10, headingLineHeight, midX, headingLineHeight+20);
            graphics2d.drawLine(midX, headingLineHeight+20, midX+10, headingLineHeight);

            // Draw the roll angle indications
            for (RollLine rollLine : rollLines){
                rollLine.draw(graphics2d, rollArcRadius, phi);
            }

            // Draw heading indicator
            graphics2d.drawLine(0, headingLineHeight, screenWidth, headingLineHeight);
            for (HeadingLine headingLine : headingLines){
                headingLine.draw(graphics2d, AsymmetricMotion.getPsi(), headingLineHeight);
            }

            //Draw the pitch symbol
            aircraftSymbol.makePitchSymbol(graphics2d);

            // Draw the flight path indication
            aircraftSymbol.drawFlightPathIndicator(graphics2d);

            // Draw the rudder indication
            rudder.draw(graphics2d);

            // Draw the thrust indication
            thrust.draw(graphics2d);

            // Draw the elevator and aileron deflections
            aileron.draw(graphics2d);
            elevator.draw(graphics2d);

            // Draw flight data
            FlightData.draw(graphics2d);

            // Write flaps
            if (SymmetricMotion.flaps_index > 0)
                graphics.drawString("Flaps: " + flaps[SymmetricMotion.flaps_index], x_write_flaps_gear, y_write_flaps);

            // Write Gear
            if (SymmetricMotion.gear)
                graphics2d.drawString("Gear", x_write_flaps_gear, y_write_gear);
        }
    }

    static void setControlMessage(String message){
        controlMessage = message;
    }

    private String getControlMessage() {
        return controlMessage;
    }

}
